var url ="./products/"
var html = document.querySelector("#produits")

fetch(url)
    .then(response => response.json())
    .then(data => {
        data.forEach(el => {
            html.innerHTML += `
            <div class="accordion" id="accordionProduct">
            <div class="card">
                <div class="card-header" id="headingProduct" style="background-color: teal;">
                    <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true" aria-controls="collapseProduct" style="color: white">
                        (${el.id}) ${el.label}
                    </button>
                    </h5>
                </div>
            
                <div id="collapseProduct" class="collapse show" aria-labelledby="headingProduct" data-parent="#accordionProduct">
                    <div class="card-body">
                        <div> ${el.description}
                        ${el.price} € <br>
                    </div>
                    <br>

                    <a class="float-right" style="color:black;" href="./produits/${el.id}">Détails</a>
                    </div>
                </div>
            </div>
            </div>
            `
        });
    })
    .catch((err) => console.log("Cannot get data" + err))

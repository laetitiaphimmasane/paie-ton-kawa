var url = "/orders/"
var html = document.querySelector("#commande")
var numProduit = window.location.pathname.split("/")[2];
var temp ="";

fetch(url + numProduit)
    .then(response => response.json())
    .then(data => {
        temp= `
            <div class="container">
            <div class="card text-center">
                <div class="card-header">
                Commande n° ${data.ref}
                </div>
                <div class="card-body">
                    <div class="card-text text-justify">
                        <div class="container">
                            <div class="row">
                `;

        data.lines.forEach(el => { 
            temp+=`
                    <div class="accordion col-6" id="accordionProduct">
            <div class="card">
                <div class="card-header" id="headingProduct" style="background-color: teal;">
                    <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true" aria-controls="collapseProduct" style="color: white">
                        Produit n° ${el.fk_product}
                    </button>
                    </h5>
                </div>            
                <div id="collapseProduct" class="collapse show" aria-labelledby="headingProduct" data-parent="#accordionProduct">
                    <div class="card-body">
                        <div> 
                        ${el.ref} ${el.libelle} <br>
                        ${el.description} <br>
                        Poids net : ${el.weight} <br>
                        Prix : ${el.price} € 
                    </div>
                    <br>
                    <a class="float-right" style="color:black;" href="./produits/${el.id}">Détails</a>
                    </div>
                </div>
            </div>
            </div>`
            });
                
        temp += `
                        <div class="col-6">
                            <p> Prix total (TVA incluse) : ${data.total_ttc} </p>
                            <p> Date de création : ${data.date_creation} </p>                                    
                            <p> Date de validation : ${data.date_validation} </p>                                    
                            <p> Date de modification : ${data.date_modification} </p>
                            <a class="float-right" style="color:black;" href="${data.online_payment_url}"> Paiement </a>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
            <div class="card-footer text-muted">
                Last update : ${data.date_creation}
                        </div>
                        </div>
                        </div>
                        `;
        html.innerHTML= temp;      
        })



        .catch((err) => console.log("Cannot get data" + err));
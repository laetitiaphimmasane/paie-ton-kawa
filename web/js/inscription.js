let result = window.location.href;
result = result.substring(result.length-3);
let username = window.location.search.split("&")[0].substring(6);
let access = false;
let users={};
let email = "";

if (result == '200') {
    
    fetch("/users")
    .then(response => response.json())
    .then(data => {
        users = data[0].members;
        for(var user in users){
            if(username.toUpperCase() == users[user].login.toUpperCase()){
                email = users[user].email;
                access=true;
            }
        }

        if (access == true){
            console.log("Bon utilisateur");

            fetch("/auth?email=" + email)
            .then(response => response.json())
            .then(data => console.log(data.message))

            document.getElementById("msg-success").innerHTML = "La clé d'accès vient de vous être envoyée sur l'adresse mail de votre compte."
            document.getElementById("btn-connexion").style.visibility="visible";
        }
        else {
            document.getElementById("msg-error").innerHTML = "Cet utilisateur est enregistré mais n'a pas de droit d'accès. Contactez l'administrateur.";
        }})
    } 
    
    else { 
        if(result == '000') { 
            document.getElementById("msg-error").innerHTML = "Un des identifiants est incorrect. Veuillez réessayer.";
        } else if (parseInt(result) >= 400) {
            document.getElementById("msg-error").innerHTML = "(" +result + ") : Un des identifiants semble incorrect. Veuillez réessayer.";
        }
    }
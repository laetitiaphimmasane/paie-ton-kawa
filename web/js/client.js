var url = "./client/"
var html = document.querySelector("#clients")

fetch(url)
    .then(response => response.json())   //.then(rep => console.log(rep))
    .then(data => {
        data.forEach(el => {
        html.innerHTML += `
        <div class="container">
        <div class="card text-center">
                <div class="card-header">
                ${el.name}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Code client : ${el.code_client} </h5>
                    <h6 class="card-title">Statut : ${el.status_prospect_label} </h5>
                    <div class="card-text text-justify" style="padding:1%">
                        <div class="container">
                            <div class="row">
                                <img class="col-6" src="${el.url}">
                                <div class="col-6">
                                    <p>Email : ${el.email} </p>
                                    <p>${el.country_code} ${el.zip} ${el.town} ${el.address}</p>
                                    <p>${el.address}</p>
                                    <a class="float-right" style="color:black;" href="./commandes/${el.id}">Voir sa commande associée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted"> ${el.ref} </div>
            </div>
            </div>
            `
        });
    })
    .catch((err) => console.log("Cannot get data" + err));
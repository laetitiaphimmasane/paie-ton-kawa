const file = document.getElementById("formTokenMd").files[0];
var qr = new QrcodeDecoder();
var email ="";
var erreur = document.getElementById("msg-error");


formTokenMd.onchange = evt => {
    const [file] = formTokenMd.files;
    if (file) {
        imageShow.src = URL.createObjectURL(file);
        qr.decodeFromImage(imageShow.src).then((res) => { //décoder le QRCode
            email = res.data; //récupérer l'email

            console.log(email);
            if (email == undefined){
                erreur.innerHTML = "La clé d'accès semble incorrecte. Vous ne pouvez pas accéder à Paie Ton Kawa.";
            } else {
                var url ="./users?email=" + email; //tester la connexion Dolibarr via les identifiants

                fetch(url).then(req => req.json())
                .then(data => {
                    if('error' in data) //gestion de l'erreur
                    {                        
                        erreur.innerHTML = "La clé d'accès semble incorrecte. Vous ne pouvez pas accéder à Paie Ton Kawa.";
                    } else {
                        window.document.location.replace('/liste-de-produits'); //redirection vers la liste des produits
                    }
                    })
            }
            });
    }
}
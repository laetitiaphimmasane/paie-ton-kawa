
import fetch from 'node-fetch';
import express from "express";
import qrcode  from 'qrcode';
import nodemailer from 'nodemailer';
import path from "path";
import dotenv from 'dotenv';
import bodyparser from 'body-parser';
import jwt from 'jsonwebtoken';


const app = express();
let __dirname = ".";

app.use(express.static('web'));
app.use(express.static(path.join(__dirname, 'web')));
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json({ type: 'application/json' }));

dotenv.config();


///////AUTHENTIFICATION

app.post("/inscription", (req, res) => { //Connexion d'un utilisateur Revendeur à Dolibarr avec username et mdp

    if(req.body.inputUsername != null && req.body.inputPassword!= null)
    {
        let id = {
            login: req.body.inputUsername,
            password: req.body.inputPassword
        }

        fetch("http://"+ process.env.ip +"/dolibarr/api/index.php/login", 
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body:JSON.stringify(id)        
            })
        .then((response) => response.json())
        .then((data) => {
            if(data.hasOwnProperty('error')){
                res.status(data.error.code);                
                res.redirect('/inscription?accept=' + data.error.code);

            } else if (data.hasOwnProperty('success')){
                res.status(200);
                res.redirect('/inscription?user=' + id.login +'&accept=200');

            }
        }).catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });

    } else {
        res.status(404)
        res.redirect("/inscription?accept=000");
    }   
});


////MIDDLEWARE/LOAD BALANCER
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    console.log(req.headers);
    console.log(process.env.privateKey);
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        console.log(token);

        jwt.verify(token, process.env.privateKey, (err, rep) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(rep);

            if('email' in rep.json() && 'revendeurs' in rep.json()){
                next(); //donner l'accès                
            }

        });
    } else {
        res.sendStatus(401);
    }
};


app.get('/auth', (req, res) => { //Création du QRCode à partir de l'email du revendeur
	
    const params = req.query;  

	if (!params) {
		return res.status(400).json({
		message: 'Bad Request: token and email are required',
		});
	}

    //Création du token
    let token = jwt.sign({ "email": params.email}, process.env.privateKey, { expiresIn: '1h'});
    console.log(token);

    jwt.verify(token, process.env.privateKey, (err, rep) => {
        console.log(rep);
    });

    token = params.email;
    qrcode.toFile("web/qrcodes/1.jpg", token);

    //Envoi du mail
	const transporter = nodemailer.createTransport({
		pool: true,
		host: 'localhost',
		port:  '25', //Port SMTP (mailing),
		auth: {
		user: process.env.identifiant,
		pass:process.env.motdepasse,
		},
		tls: {
		    rejectUnauthorized: false,  // do not fail on invalid certs
		}
	});
    const mailOptions = {
        from: 'admin@Top1.com',
        to: params.email,
        subject: 'QR Code for authentification',
        html: '<p>Hello, Please use the following QR Code to authenticate your account:</p> <br> <img src="cid:qrcode@revendeur"/>',
        attachments: [{
        filename: 'qrcode.png',
        path: 'web/qrcodes/1.jpg',
        cid: 'qrcode@revendeur' //same cid value as in the html img src
    }]

    };
    
	const mail = transporter.sendMail(mailOptions);

	if (mail) {
		res.send({ message: 'Email sent successfully' });
	} else {
		res.status(500).send({ message: 'An error occured' });
	} 
});


///////ROUTAGE

app.get('/', (req, res) => {
    console.log(+ req.socket.localAddress);
    res.sendFile('/web/index.html', { root: '.' });
})

app.get('/inscription', (req, res) => {
    res.sendFile('/web/inscription.html', { root: '.' });
})

app.get('/connexion', (req, res) => {
    res.sendFile('/web/connexion.html', { root: '.' });
})




app.get('/liste-de-produits', (req, res) => {
    res.sendFile('/web/produits.html', { root: '.' });
})

app.get('/produits/:id', (req, res) => {
    res.sendFile('/web/detail_produit.html', { root: '.' });
});


app.get('/qrcode', (req, res) => {
    res.sendFile('/web/qrcodes/1.jpg', {root: '.'});
})

app.get('/accompagnement' , (req, res) => {
    res.sendFile('/web/contact.html', {root: '.'});
})

app.get('/commandes/:id',   (req, res) => {
    res.sendFile('/web/commandes.html', { root: '.' });
})

app.get('/liste-de-clients', (req, res) => {
    res.sendFile('/web/client.html', { root: '.' });
})




///////REQUETES API
//Variables
const uri = "http://"+ process.env.ip +"/dolibarr/api/index.php";
const apiKey = "?DOLAPIKEY=test";


/////API REVENDEURS
app.get("/products", (req, res) => {
    const productsFetch = fetch(uri + "/products" + apiKey);
    productsFetch.then((response) => response.json())
    .then((data) => res.json(data))
    .catch(err => {
        res.writeHead(500);
        res.end(err);
        return
    });
});

app.get("/products/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const product = fetch(uri + "/products/"+ id + apiKey);

    if (product){
        product.then((response) => response.json())
        .then((data) => res.json(data))
        .catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });
    } else {
        res.status(404).send("Produit "+ id + " non trouvé");
    }
});


app.get("/documents/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const product = fetch(uri + "/documents" + apiKey +"&modulepart=product&id="+ id );

    if (product){
        product.then((response) => response.json())
        .then((data) => res.json(data))
        .catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });
    } else {
        res.status(404).send("Document produit "+ id + " non trouvé");
    }
});


app.get("/users/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const product = fetch(uri + "/users/"+ id + apiKey);

    if (product){
        product.then((response) => response.json())
        .then((data) => res.json(data))
        .catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });
    } else {
        res.status(404).send("Client "+ id + " non trouvé");
    }
});


app.get("/users/:email", (req, res) => {
    const email = parseInt(req.params.email);
    const access = fetch(uri + "/users/email/"+ email + apiKey);

    if (access){
        access.then((response) => response.json())
        .then((data) => res.json(data))
        .catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });
    } else {
        res.status(404).send("Utilisateur "+ id + " non trouvé");
    }
});

app.get("/users", (req, res) => {
    const productsFetch = fetch(uri + "/users/groups" + apiKey);

    productsFetch.then((response) => response.json())
    .then((data) => res.json(data))
    .catch(err => {
        res.writeHead(500);
        res.end(err);
        return
    });
});


/////API WEBSHOP
app.get("/client", (req, res) => {
    const productsFetch = fetch(uri + "/thirdparties" + apiKey);

    productsFetch.then((response) => response.json())
    .then((data) => res.json(data))
    .catch(err => {
        res.writeHead(500);
        res.end(err);
        return
    });
});

app.get("/orders/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const order = fetch(uri + "/orders/"+ id + apiKey);

    if (order) {
        order.then((response) => response.json())
        .then((data) => res.json(data))
        .catch(err => {
            res.writeHead(500);
            res.end(err);
            return
        });
    } else{
        res.status(404).send("Commande du client "+ id + " non trouvée");
    }

});


//Gestion des erreurs
app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain; charset=UTF-8') ;
    res.status(404).send('Page introuvable') ;
})

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send(err.stack);
    return next();
});


//Ouverture du serveur
app.listen(process.env.port, () => {
    console.log(`Paie ton kawa is listening on port ${process.env.port} `)
})